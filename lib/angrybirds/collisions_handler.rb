module Angrybirds
  # handler for collisions
  class CollisionsHandler
    def initialize(space)
      space.add_collision_handler(:pig, :block, self)
      space.add_collision_handler(:pig, :floor, self)
      space.add_collision_handler(:pig, :bird, self)
      space.add_collision_handler(:pig, :pig, self)
      space.add_collision_handler(:block, :block, self)
      space.add_collision_handler(:block, :bird, self)
      space.add_collision_handler(:block, :floor, self)
      space.add_collision_handler(:bird, :floor, self)
    end

    def post_solve(shape1, shape2, arbiter)
      force = arbiter.impulse.length

      return unless arbiter.first_contact? && force > 10

      shape1.object.hit(force)
      shape2.object.hit(force)
    end
  end
end
