require 'rspec'
require 'gosu'
require 'chipmunk'
require_relative '../lib/angrybirds/pig.rb'
require_relative '../lib/angrybirds/game.rb'

describe Angrybirds::Pig do
  let(:pig_x) { 3.0 }
  let(:pig_y) { 2.0 }
  let(:window) { Angrybirds::Game.new(10, 10) }
  subject(:pig) { described_class.new(window, pig_x, pig_y) }

  context 'pig' do
    it 'correctly sets position' do
      expect(pig.center_x).equal? pig_x
      expect(pig.center_y).equal? pig_y
    end
  end

  context 'hitting' do
    it 'changes life points' do
      prev_points = pig.life_points
      expect(pig.hit(described_class::MAX_LIFE_POINTS))
      expect(pig.life_points).to be < prev_points
    end

    it 'changes damage level' do
      prev_level = pig.damage_level
      expect(pig.hit(described_class::MAX_LIFE_POINTS / 2))
      expect(pig.damage_level).to be > prev_level
    end

    it 'may destroy the pig' do
      expect(pig.destroyed?).to be false
      pig.hit(described_class::MAX_LIFE_POINTS + 1)
      expect(pig.destroyed?).to be true
    end
  end

  context 'pig' do
    it 'draws a nice pig without error' do
      expect(pig.draw)
    end

    it 'can be updated without error' do
      expect(pig.update)
    end
  end
end
