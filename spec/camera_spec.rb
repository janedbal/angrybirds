require 'rspec'
require 'gosu'
require 'chipmunk'
require_relative '../lib/angrybirds/camera.rb'
require_relative '../lib/angrybirds/game.rb'

describe Angrybirds::Camera do
  let(:window) { Gosu::Window.new(10, 10, false) }
  subject(:camera) { described_class.new(window) }

  context 'camera init' do
    it 'creates non moving camera' do
      expect(camera.moving).to be false
    end
  end

  context 'moving' do
    it 'can start and stop' do
      expect(camera.moving).to be false
      expect(camera.start_moving)
      expect(camera.moving).to be true
      expect(camera.end_moving)
      expect(camera.moving).to be false
    end

    it 'towards some point changes position' do
      old_x = camera.x
      old_y = camera.y
      camera.move_camera_towards(10, 10)
      expect(camera.x).not_to be old_x
      expect(camera.y).not_to be old_y
    end
  end

  context 'camera' do
    it 'can be updated without error' do
      expect(camera.update)
    end
  end
end
