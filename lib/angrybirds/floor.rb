
module Angrybirds
  # static floor
  class Floor
    INFINITY = 1.0 / 0
    FLOOR_BOTTOM_OFFSET = 200

    def initialize(window)
      @window = window

      @a = CP::Vec2.new(0, 0)
      @b = CP::Vec2.new(Game::SPACE_WIDTH, 0)

      init_body
      init_shape

      @color = Gosu::Color::BLACK
      @window.space.add_static_shape(@shape)
    end

    def init_body
      @body = CP::Body.new(INFINITY, INFINITY)
      @body.p = CP::Vec2.new(0, Game::SPACE_HEIGHT - Floor::FLOOR_BOTTOM_OFFSET)
    end

    def init_shape
      @shape = CP::Shape::Segment.new(@body, @a, @b, 1)
      @shape.u = 0.5 # friction
      @shape.e = 0.7 # elasticity
      @shape.collision_type = :floor
      @shape.object = self
    end

    def hit(_damage)
      # ignore
    end
  end
end
