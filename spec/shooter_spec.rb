require 'rspec'
require 'gosu'
require 'chipmunk'
require_relative '../lib/angrybirds/shooter.rb'
require_relative '../lib/angrybirds/game.rb'

describe Angrybirds::Shooter do
  let(:window) { Angrybirds::Game.new(10, 10) }
  subject(:shooter) { described_class.new(window) }

  context 'shooter' do
    it 'draws a nice shooter without error' do
      expect(shooter.draw)
    end

    it 'can be updated without error' do
      expect(shooter.update)
    end
  end
end
