
module Angrybirds
  # standard bird "missile" class
  class Bird
    attr_reader :fired, :shape, :body, :crashed

    CENTER_OFFSET = 25

    def initialize(window, x, y)
      @window = window
      @img = Gosu::Image.new(window, File.dirname(__FILE__) + '/media/red.png')
      @fired = false
      @crashed = false

      init_body(x, y)
      init_shape
    end

    def init_body(x, y)
      @mass = 10
      @inertia = 5000 # body's resistance to change in rotation
      @body = CP::Body.new(@mass, @inertia)
      @body.p = CP::Vec2.new(x, y)
      @body.v = CP::Vec2.new(0, 0)
    end

    def init_shape
      @radius = 19
      @shape = CP::Shape::Circle.new(@body, @radius, CP::Vec2.new(0, 0))
      @shape.u = 0.5 # friction
      @shape.e = 0.7 # elasticity
      @shape.collision_type = :bird
      @shape.object = self
    end

    def move_center(x, y)
      @body.p.x = x
      @body.p.y = y
    end

    def fire(power, angle)
      @fired = true

      @window.space.add_body(@body)
      @window.space.add_shape(@shape)

      force = CP::Vec2.for_angle(angle) * -power
      force_source = @body.p # source of impulse coming from center of bird
      @body.apply_impulse(force, force_source)
      @body.w = 0 # no rotational velocity after force being applied
    end

    def hit(_damage)
      @crashed = true unless @crashed
    end

    def center_x
      @body.p.x
    end

    def center_y
      @body.p.y
    end

    def draw
      @img.draw_rot(center_x, center_y, 0, @body.a.radians_to_degrees)
    end

    def update
      @body.w *= 0.99
      @body.reset_forces
    end

    def should_be_destroyed?
      (@fired && @body.v.length < 1) ||
        @body.p.x < 0 ||
        @body.p.x > Game::SPACE_WIDTH
    end
  end
end
