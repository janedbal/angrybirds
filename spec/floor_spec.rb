require 'rspec'
require 'gosu'
require 'chipmunk'
require_relative '../lib/angrybirds/floor.rb'
require_relative '../lib/angrybirds/game.rb'

describe Angrybirds::Floor do
  let(:window) { Angrybirds::Game.new(10, 10) }
  subject(:floor) { described_class.new(window) }

  context 'floor' do
    it 'hitting does nothing' do
      expect(floor.hit(10))
    end
  end
end
