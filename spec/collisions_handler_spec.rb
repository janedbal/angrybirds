require 'rspec'
require 'gosu'
require 'chipmunk'
require_relative '../lib/angrybirds/collisions_handler.rb'
require_relative '../lib/angrybirds/game.rb'

describe Angrybirds::CollisionsHandler do
  context 'initialization' do
    it 'setups handlers' do
      double = double
      double.should_receive(:add_collision_handler).at_least(:once)
      expect(described_class.new(double))
    end
  end
end
