
module Angrybirds
  # map loading and saving
  class MapLoader
    def self.save(filename, blocks, pigs)
      contents = StringIO.new

      blocks.each do |block|
        contents << 'block;'
        contents << block.center_x.to_s << ';'
        contents << block.center_y.to_s << ';'
        contents << block.material.to_s << ';'
        contents << block.type.to_s << ';'
        contents << block.rotation.to_s << "\n"
      end

      pigs.each do |pig|
        contents << 'pig;'
        contents << pig.center_x.to_s << ';'
        contents << pig.center_y.to_s << "\n"
      end

      file = File.dirname(__FILE__) + "/maps/#{filename}.map"
      File.write(file, contents.string)
    end

    def self.load(level, window)
      contents = File.read(map_filename(level))

      pigs = []
      blocks = []
      lines = contents.split("\n")

      lines.each do |line|
        parts = line.split(';')
        type = parts[0]

        if type == 'block'
          blocks.push Block.new(window, parts[1].to_f, parts[2].to_f,
                                parts[3], parts[4], parts[5].to_f)
        elsif type == 'pig'
          pigs.push Pig.new(window, parts[1].to_f, parts[2].to_f)
        end
      end
    end

    def self.level_exists?(level)
      File.exist?(map_filename(level))
    end

    def self.map_filename(level)
      File.dirname(__FILE__) + "/maps/#{level.to_s.rjust(2, '0')}.map"
    end
  end
end
