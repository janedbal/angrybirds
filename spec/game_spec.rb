require 'rspec'
require 'gosu'
require 'chipmunk'
require_relative '../lib/angrybirds/game.rb'

describe Angrybirds::Game do
  subject(:game) { described_class.new(10, 10) }

  context 'initialize' do
    it 'setups birds' do
      expect(game.birds.count).equal? Angrybirds::Game::MAX_BIRDS
    end

    it 'setups pigs' do
      expect(game.pigs.count).to be > 0
    end

    it 'setups blocks' do
      expect(game.blocks.count).to be > 0
    end

    it 'without any fired bird' do
      expect(game.bird_last_fired).to be nil
      expect(game.birds_remaining).equal? Angrybirds::Game::MAX_BIRDS
    end

    it 'first level' do
      expect(game.bird_last_fired).equal? 1
    end

    it 'some space' do
      expect(game.space).not_to be nil
    end
  end

  context 'firing' do
    it 'setups last fired bird' do
      game.fire
      expect(game.bird_last_fired).not_to be nil
    end
  end

  context 'game' do
    it 'can be updated without error' do
      expect(game.update)
    end

    it 'can be drawed without error' do
      expect(game.draw)
    end
  end

  context 'needs' do
    it 'cursor' do
      expect(game.needs_cursor?).to be true
    end
  end
end
