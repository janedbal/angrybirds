
require 'rubygems'
require 'stringio'
require 'gosu'
require 'chipmunk'

require_relative 'floor'
require_relative 'builder'
require_relative 'pig'
require_relative 'bird'
require_relative 'explosion'
require_relative 'shooter'
require_relative 'trace_cloud'
require_relative 'game_info'
require_relative 'block'
require_relative 'camera'
require_relative 'map_loader'
require_relative 'collisions_handler'

# The Game module
module Angrybirds
  # Angry Birds window
  class Game < Gosu::Window
    attr_reader :birds, :pigs, :blocks, :birds_remaining,
                :level, :space, :bird_last_fired

    MAX_BIRDS = 5
    SPACE_WIDTH = 2560
    SPACE_HEIGHT = 1440
    WINDOW_WIDTH = 1280
    WINDOW_HEIGHT = 846
    SUBSTEPS = 10

    def initialize(width = Gosu.screen_width, height = Gosu.screen_height)
      super(width, height, false)
      self.caption = 'Angry Birds'

      @dt = (1.0 / 60.0)
      @space = CP::Space.new
      @space.gravity = CP::Vec2.new(0, 10)
      @space.damping = 0.99 # little slow down when moving through space

      @level = 1
      @iteration = 0
      @new_bird_timeout = 300
      @traces = []
      @birds = []
      @blocks = []
      @bird_last_fired = nil
      @pigs = []
      @explosions = []
      @birds_remaining = Game::MAX_BIRDS
      @camera = Camera.new(self)

      @builder = Builder.new(self)
      @floor = Floor.new(self)
      @info = GameInfo.new(self)
      bgfile = File.dirname(__FILE__) + '/media/bg.jpg'
      @background = Gosu::Image.new(self, bgfile)
      @shooter = Shooter.new(self)
      @last_milliseconds = 0
      @birds.push(Bird.new(self, Shooter::POS_X, Shooter::POS_Y))

      MapLoader.load(@level, self)

      CollisionsHandler.new(@space)
    end

    def needs_cursor?
      true
    end

    def camera_mouse_x
      mouse_x + @camera.x
    end

    def camera_mouse_y
      mouse_y + @camera.y
    end

    def draw
      translate(-@camera.x, -@camera.y) do
        @background.draw(0, 0, 0)
        @pigs.each(&:draw)
        @traces.each(&:draw)
        @shooter.draw
        @birds.each(&:draw)
        @blocks.each(&:draw)
        @explosions.each(&:draw)
        @builder.draw
      end

      @info.draw
    end

    def button_down(key)
      close if key == Gosu::KbEscape

      @builder.button_down(key)
      shooting = @shooter.button_down(key)
      @camera.start_moving if !shooting && key == Gosu::MsLeft

      next_level if @info.info && key == Gosu::MsLeft
    end

    def button_up(key)
      @shooter.button_up(key)
      @camera.end_moving if key == Gosu::MsLeft && @camera.moving
    end

    def fire
      @traces.clear

      @camera.target_bird
      @bird_last_fired = @birds.last
      @new_bird_timeout = 50
    end

    def update
      update_delta
      update_pigs
      update_birds
      update_traces
      update_info
      update_blocks
      update_explosions

      @shooter.update
      @camera.update

      update_space
    end

    private

    def update_space
      Game::SUBSTEPS.times do
        @space.step(@dt)
      end
    end

    def update_info
      return unless @birds_remaining == 0 && @birds.empty? && !@pigs.empty?

      end_level(false)
    end

    def update_blocks
      @blocks.delete_if do |block|
        if block.destroyed?
          @explosions.push(Explosion.new(self, block.center_x, block.center_y))
          delete_block(block)
          true
        else
          block.update
          false
        end
      end
    end

    def update_pigs
      @pigs.delete_if do |pig|
        if pig.destroyed?
          @explosions.push(Explosion.new(self, pig.center_x, pig.center_y))
          delete_pig(pig)
        else
          pig.update
          false
        end
      end

      end_level(true) if @pigs.empty? && !@info.info
    end

    def update_birds
      @birds.delete_if do |bird|
        if bird.should_be_destroyed?
          delete_bird(bird)
        else
          bird.update
          false
        end
      end

      check_new_bird

      @new_bird_timeout -= 1 if !@birds.empty? && @birds.last.fired
    end

    def update_traces
      return unless @bird_last_fired &&
                    !@bird_last_fired.crashed &&
                    @iteration % 4 == 0

      x = @bird_last_fired.center_x
      y = @bird_last_fired.center_y
      @traces.push TraceCloud.new(self, x, y)
    end

    def update_explosions
      @explosions.each do |explosion|
        if explosion.finished?
          @explosions.delete(explosion)
        else
          explosion.update
        end
      end
    end

    def update_delta
      current_time = Gosu.milliseconds / 1000.0
      @delta = [current_time - @last_milliseconds, 0.25].min
      @last_milliseconds = current_time
      @iteration += 1
    end

    def check_new_bird
      return unless @birds_remaining > 0 &&
                    (@birds.empty? ||
                        (@birds.last.fired && @new_bird_timeout == 0))

      @birds.push(Bird.new(self, Shooter::POS_X, Shooter::POS_Y))
      @birds_remaining -= 1
      @shooter.allow_shooting
    end

    def delete_pig(pig)
      @space.remove_shape(pig.shape)
      @space.remove_body(pig.body)
    end

    def delete_bird(bird)
      if bird == @bird_last_fired
        @camera.reset_speed
        @bird_last_fired = nil
      end

      @space.remove_shape(bird.shape)
      @space.remove_body(bird.body)
    end

    def delete_block(block)
      @space.remove_shape(block.shape)
      @space.remove_body(block.body)
    end

    def end_level(success)
      return if @builder.enabled

      @info.end_level(success)
      @level += 1 if success
    end

    def start_level
      @bird_last_fired = nil
      @blocks.each { |block| delete_block(block) }
      @birds.each { |bird| delete_bird(bird) }
      @pigs.each { |pig| delete_pig(pig) }

      @traces.clear
      @blocks.clear
      @birds.clear
      @pigs.clear

      @birds_remaining = Game::MAX_BIRDS
      MapLoader.load(@level, self)
    end

    def next_level
      if MapLoader.level_exists?(@level)
        @info.dismiss_box
        start_level
      else
        close
      end
    end
  end
end
