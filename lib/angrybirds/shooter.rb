
module Angrybirds
  # for shooting birds
  class Shooter
    POS_X = 300
    POS_Y = 1023
    POWER_MAX = 1200

    def initialize(window)
      imgfile = File.dirname(__FILE__) + '/media/shooter.png'
      @window = window
      @img = Gosu::Image.new(window, imgfile)
      @shooting = false
      @shooting_allowed = true
    end

    def allow_shooting
      @shooting_allowed = true
    end

    def draw
      @img.draw(Shooter::POS_X, Shooter::POS_Y, 0)

      return unless @shooting_allowed && !@window.birds.empty?

      draw_sling
    end

    def draw_sling
      color = Gosu::Color::BLACK
      to_x = @window.birds.last.center_x - 10
      to_y = @window.birds.last.center_y

      (0..5).each do |i|
        from_y = Shooter::POS_Y + 30 + i
        @window.draw_line(Shooter::POS_X + 5, from_y, color, to_x, to_y, color)
        @window.draw_line(Shooter::POS_X + 55, from_y, color, to_x, to_y, color)
      end
    end

    def button_down(key)
      if key == Gosu::MsLeft && @shooting_allowed && distance < 100
        @shooting = true
      end

      @shooting
    end

    def button_up(key)
      return unless key == Gosu::MsLeft && @shooting

      @window.fire
      @window.birds.last.fire(power, angle)
      @shooting = false
      @shooting_allowed = false
    end

    def update
      return unless @shooting_allowed && @shooting && !@window.birds.empty?

      if distance < 100
        x = @window.camera_mouse_x
        y = @window.camera_mouse_y
      else
        x = Shooter::POS_X + Bird::CENTER_OFFSET + delta_x / (distance / 100)
        y = Shooter::POS_Y + Bird::CENTER_OFFSET + delta_y / (distance / 100)
      end

      @window.birds.last.move_center(x, y)
    end

    private

    def distance
      Math.sqrt(delta_x**2 + delta_y**2)
    end

    def power
      ([distance, 100].min / 100) * Shooter::POWER_MAX
    end

    def angle
      Math.atan2(delta_y, delta_x)
    end

    def delta_x
      @window.camera_mouse_x - Shooter::POS_X - Bird::CENTER_OFFSET
    end

    def delta_y
      @window.camera_mouse_y - Shooter::POS_Y - Bird::CENTER_OFFSET
    end
  end
end
