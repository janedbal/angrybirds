require 'rspec'
require 'gosu'
require 'chipmunk'
require_relative '../lib/angrybirds/trace_cloud.rb'
require_relative '../lib/angrybirds/game.rb'

describe Angrybirds::TraceCloud do
  let(:window) { Gosu::Window.new(10, 10, false) }
  subject(:trace_cloud) { described_class.new(window, 3, 3) }

  context 'trace_cloud' do
    it 'draws a nice cloud without error' do
      expect(trace_cloud.draw)
    end
  end
end
