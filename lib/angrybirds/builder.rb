
module Angrybirds
  # allows to create maps (class for developing)
  class Builder
    attr_reader :enabled

    MATERIALS = [:stone, :wood, :glass]
    TYPES = [:rock, :triangle, :square, :long, :medium]

    def initialize(window)
      @window = window
      @enabled = false
      @pig_mode = false
      @material = 0
      @type = 0
      @rotation = 0
      refresh_block_img
    end

    def draw
      x = @window.camera_mouse_x
      y = @window.camera_mouse_y
      @img.draw_rot(x, y, 0, @rotation) if @enabled
    end

    def current_material
      Builder::MATERIALS[@material]
    end

    def current_type
      Builder::TYPES[@type]
    end

    def button_down(key)
      @enabled = !@enabled if key == Gosu::KbB

      return unless @enabled

      case key
      when Gosu::MsLeft
        mouse_left

      when Gosu::KbUp
        @material = (@material + 1) % Builder::MATERIALS.count

      when Gosu::KbDown
        @material = (@material - 1) % Builder::MATERIALS.count

      when Gosu::KbLeft
        @type = (@type - 1) % Builder::TYPES.count

      when Gosu::KbRight
        @type = (@type + 1) % Builder::TYPES.count

      when Gosu::MsWheelUp
        @rotation = (@rotation + 5) % 360

      when Gosu::MsWheelDown
        @rotation = (@rotation - 5) % 360

      when Gosu::KbS
        save_map

      when Gosu::KbS
        pig_move
      end

      refresh_block_img unless @pig_mode
    end

    private

    def save_map
      filename = ('a'..'z').to_a.shuffle.first(8).join
      MapLoader.save(filename, @window.blocks, @window.pigs)
    end

    def pig_mode
      @pig_mode = !@pig_mode
      return unless @pig_mode

      file = File.dirname(__FILE__) + '/media/pig-1.png'
      @rotation = 0
      @img = Gosu::Image.new(@window, file)
    end

    def mouse_left
      if @pig_mode
        Pig.new(@window, @window.camera_mouse_x, @window.camera_mouse_y)
      else
        Block.new(@window, @window.camera_mouse_x, @window.camera_mouse_y,
                  current_material, current_type, @rotation)
      end
    end

    def refresh_block_img
      blockname = "block-#{current_material}-#{current_type}.png"
      filename = File.dirname(__FILE__) + "/media/#{blockname}"
      @img = Gosu::Image.new(@window, filename) if File.exist?(filename)
    end
  end
end
