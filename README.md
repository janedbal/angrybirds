# Angrybirds

Simple angry birds game implemented in gosu and chipmunk

## Installation

```sh
gem install bundler
bundle install
```

## Run

```sh
bin/angrybirds
```

## Rebuild

```sh
rake install
```

## Run tests

```sh
rspec spec
```

## Tested on

    - ruby 1.9.3p551 (2014-11-13) [i386-mingw32]
    - gosu (0.8.7.2 x86-mingw32)
    - chipmunk (6.1.3.4) 
