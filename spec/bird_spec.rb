require 'rspec'
require 'gosu'
require 'chipmunk'
require_relative '../lib/angrybirds/bird.rb'
require_relative '../lib/angrybirds/game.rb'

describe Angrybirds::Bird do
  context 'bird' do
    let(:bird_x) { 3.0 }
    let(:bird_y) { 2.0 }
    subject(:window) { Gosu::Window.new(10, 10, false) }
    subject(:bird) { Angrybirds::Bird.new(window, bird_x, bird_y) }

    it 'correctly sets position' do
      expect(bird.center_x).equal? bird_x
      expect(bird.center_y).equal? bird_y
    end

    it 'correctly changes position' do
      new_x = 6
      new_y = 8
      bird.move_center(new_x, new_y)
      expect(bird.center_x).equal? new_x
      expect(bird.center_y).equal? new_y
    end

    it 'hitting changes crashed flag' do
      expect(bird.crashed).to be false
      expect(bird.hit(10))
      expect(bird.crashed).to be true
    end

    it 'agrees to be destroyed' do
      expect(bird.should_be_destroyed?).to be false
      bird.move_center(-5, -5)
      expect(bird.should_be_destroyed?).to be true
    end

    it 'draws a nice bird without error' do
      expect(bird.draw)
    end

    it 'can be updated without error' do
      expect(bird.update)
    end
  end
end
