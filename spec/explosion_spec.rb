require 'rspec'
require 'gosu'
require 'chipmunk'
require_relative '../lib/angrybirds/explosion.rb'

describe Angrybirds::Explosion do
  let(:explosion_x) { 3.0 }
  let(:explosion_y) { 2.0 }
  let(:window) { Gosu::Window.new(10, 10, false) }
  subject(:explosion) { described_class.new(window, explosion_x, explosion_y) }

  context 'explosion init' do
    it 'creates non-finished explosion' do
      expect(explosion.finished?).to be false
    end
  end

  context 'finishing' do
    it 'may occur after many updates' do
      expect(explosion.finished?).to be false

      24.times { explosion.update }

      expect(explosion.finished?).to be true
    end
  end

  context 'explosion' do
    it 'can be updated without error' do
      expect(explosion.update)
    end

    it 'can draw an explosion without error' do
      expect(explosion.draw)
    end
  end
end
