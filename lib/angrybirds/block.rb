
module Angrybirds
  # game block
  class Block
    attr_reader :shape, :body, :material, :type, :life_points

    VERTS = {
      rock: [
        CP::Vec2.new(-39, -2),
        CP::Vec2.new(-28, 29),
        CP::Vec2.new(9, 37),
        CP::Vec2.new(34, 20),
        CP::Vec2.new(37, -8),
        CP::Vec2.new(11, -36),
        CP::Vec2.new(-21, -34)
      ],
      triangle: [
        CP::Vec2.new(-42, 42),
        CP::Vec2.new(42, 42),
        CP::Vec2.new(-42, -42)
      ],
      square: [
        CP::Vec2.new(-42, 42),
        CP::Vec2.new(42, 42),
        CP::Vec2.new(42, -42),
        CP::Vec2.new(-42, -42)
      ],
      long: [
        CP::Vec2.new(-84.5, 11),
        CP::Vec2.new(84.5, 11),
        CP::Vec2.new(84.5, -11),
        CP::Vec2.new(-84.5, -11)
      ],
      medium: [
        CP::Vec2.new(-42.5, 11),
        CP::Vec2.new(42.5, 11),
        CP::Vec2.new(42.5, -11),
        CP::Vec2.new(-42.5, -11)
      ]
    }

    MASS = {
      rock: 15,
      triangle: 15,
      square: 18,
      long: 10,
      medium: 6
    }

    LIFE_POINTS = {
      rock: 2300,
      triangle: 2600,
      square: 2600,
      long: 1500,
      medium: 800
    }

    def initialize(window, x, y, material, type, rotation)
      file = File.dirname(__FILE__) + "/media/block-#{material}-#{type}.png"
      @img = Gosu::Image.new(window, file)

      @type = type.to_sym
      @material = material.to_sym

      init_body(x, y, rotation)
      init_shape

      window.space.add_body(@body)
      window.space.add_shape(@shape)
      window.blocks.push self
    end

    def init_body(x, y, rotation)
      mass = Block::MASS[@type]
      mass /= 2 if @material == :wood
      mass /= 4 if @material == :glass

      life_points = Block::LIFE_POINTS[@type]
      life_points /= 2 if @material == :wood
      life_points /= 4 if @material == :glass

      @mass = mass
      @life_points = life_points
      @inertia = 5000
      @body = CP::Body.new(@mass, @inertia)
      @body.p = CP::Vec2.new(x, y)
      @body.v = CP::Vec2.new(0, 0)
      @body.w = 0
      @body.a = rotation.degrees_to_radians
    end

    def init_shape
      verts = Block::VERTS[@type]

      @shape = CP::Shape::Poly.new(@body, verts, CP::Vec2.new(0, 0))
      @shape.u = 1
      @shape.e = 0.5
      @shape.collision_type = :block
      @shape.object = self
    end

    def center_x
      @body.p.x
    end

    def center_y
      @body.p.y
    end

    def rotation
      @body.a.radians_to_degrees
    end

    def draw
      @img.draw_rot(@body.p.x, @body.p.y, 0, @body.a.radians_to_degrees)
    end

    def hit(damage)
      @life_points -= damage
    end

    def damage_level
      5 - (@life_points / (Pig::MAX_LIFE_POINTS / 4)).ceil
    end

    def destroyed?
      @life_points < 0
    end

    def update
      @body.reset_forces
    end
  end
end
