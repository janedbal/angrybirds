require 'rspec'
require 'gosu'
require 'chipmunk'
require_relative '../lib/angrybirds/game_info.rb'
require_relative '../lib/angrybirds/game.rb'

describe Angrybirds::GameInfo do
  let(:window) { Angrybirds::Game.new(10, 10) }
  subject(:game_info) { described_class.new(window) }

  context 'info box' do
    it 'is created at the level end' do
      expect(game_info.info).to be nil
      expect(game_info.end_level(true))
      expect(game_info.info).not_to be nil
    end

    it 'may be dismissed' do
      expect(game_info.end_level(false))
      expect(game_info.info).not_to be nil
      expect(game_info.dismiss_box)
      expect(game_info.info).to be nil
    end
  end

  context 'game_info' do
    it 'draws some info without error' do
      expect(game_info.draw)
    end
  end
end
