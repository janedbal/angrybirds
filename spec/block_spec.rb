require 'rspec'
require 'gosu'
require 'chipmunk'
require_relative '../lib/angrybirds/block.rb'
require_relative '../lib/angrybirds/game.rb'

describe Angrybirds::Block do
  let(:block_x) { 3.0 }
  let(:block_y) { 2.0 }
  let(:material) { :stone }
  let(:type) { :square }
  let(:window) { Angrybirds::Game.new(10, 10) }
  subject(:block) do
    described_class.new(window, block_x, block_y, material, type, 0)
  end

  context 'block init' do
    it 'correctly sets position' do
      expect(block.center_x).equal? block_x
      expect(block.center_y).equal? block_y
    end

    it 'correctly sets material' do
      expect(block.material).equal? material
    end

    it 'correctly sets type' do
      expect(block.type).equal? type
    end
  end

  context 'hitting' do
    it 'changes life points' do
      prev_points = block.life_points
      expect(block.hit(100))
      expect(block.life_points).to be < prev_points
    end
  end

  context 'block' do
    it 'may be destroyed' do
      expect(block.destroyed?).to be false
      block.hit(100_000)
      expect(block.destroyed?).to be true
    end

    it 'draws a nice block without error' do
      expect(block.draw)
    end

    it 'can be updated without error' do
      expect(block.update)
    end
  end
end
