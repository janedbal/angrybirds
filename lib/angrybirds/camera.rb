
module Angrybirds
  # space view class
  class Camera
    DEFAULT_SPEED = 2.5
    MAX_SPEED = 25

    attr_reader :x, :y, :moving

    def initialize(window)
      @window = window
      @moving = false
      @target_bird = false
      @x_max = Game::SPACE_WIDTH - @window.width
      @y_max = Game::SPACE_HEIGHT - @window.height

      @x = 0
      @y = @y_max

      reset_speed
    end

    def target_bird
      @target_bird = true
    end

    def reset_speed
      @speed = Camera::DEFAULT_SPEED
    end

    def start_moving
      @moving = true
      @moving_start_x = @x
      @moving_start_y = @y
      @moving_start_mouse_x = @window.mouse_x
      @moving_start_mouse_y = @window.mouse_y
    end

    def end_moving
      @moving = false
      @target_bird = false
    end

    def update
      update_manual_move if @moving

      if @target_bird && @window.bird_last_fired
        target_x = @window.bird_last_fired.center_x - @window.width / 2
        target_y = @window.bird_last_fired.center_y - @window.height / 2
      else
        target_x = Shooter::POS_X - @window.width / 2
        target_y = Shooter::POS_Y - @window.height / 2
      end

      move_camera_towards(target_x, target_y)
    end

    def update_manual_move
      @x = @moving_start_x + @moving_start_mouse_x - @window.mouse_x
      @y = @moving_start_y + @moving_start_mouse_y - @window.mouse_y
      fix_camera
    end

    def move_camera_towards(tx, ty)
      target = CP::Vec2.new(tx, ty)
      old_pos = CP::Vec2.new(@x, @y)
      distance = old_pos.dist(target)
      accelerate = 1 + distance / 20_000
      speed = [distance, @speed * accelerate, Camera::MAX_SPEED].min

      new_pos = old_pos + (target - old_pos).normalize * speed

      @x = new_pos.x
      @y = new_pos.y
      fix_camera

      @speed = [CP::Vec2.new(@x, @y).dist(old_pos), Camera::DEFAULT_SPEED].max
    end

    def fix_camera
      @x = 0 if @x < 0
      @y = 0 if @y < 0

      @x = @x_max if @x > @x_max
      @y = @y_max if @y > @y_max
    end
  end
end
