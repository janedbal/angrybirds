
module Angrybirds
  # cloud created behind bird fly
  class TraceCloud
    def initialize(window, x, y)
      cloudes = {
        0 => 'mini',
        1 => 'micro',
        2 => 'nano'
      }
      type = rand(0..2)
      file = File.dirname(__FILE__) + "/media/cloud-#{cloudes[type]}.png"
      @img = Gosu::Image.new(window, file)

      @x = x
      @y = y
    end

    def draw
      @img.draw(@x, @y, 0)
    end
  end
end
