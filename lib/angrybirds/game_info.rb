
module Angrybirds
  # information about game progress
  class GameInfo
    attr_reader :info

    LEFT_OFFSET = 10

    def initialize(window)
      @window = window
      @font = Gosu::Font.new(window, 'Arial', 20)
      @font_huge = Gosu::Font.new(window, 'Arial', 40)
      @img = Gosu::Image.new(window, File.dirname(__FILE__) + '/media/info.png')
      @info = nil
      @info_color = nil
    end

    def draw
      white = Gosu::Color::WHITE
      level = @window.level
      pigs = @window.pigs.count
      birds = @window.birds_remaining
      left = GameInfo::LEFT_OFFSET
      top = @window.height - Floor::FLOOR_BOTTOM_OFFSET + 100
      draw_text(@font, "Level: #{level}", left, top, white)
      draw_text(@font, "Pigs remaining: #{pigs}", left, top + 25, white)
      draw_text(@font, "Birds remaining: #{birds}", left, top + 50, white)

      draw_box if @info
    end

    def draw_box
      box_x = @window.width / 2 - @img.width / 2
      box_y = @window.height / 2 - @img.height / 2
      text_x = @window.width / 2 - 120
      text_y = @window.height / 2 - 20
      @img.draw(box_x, box_y, 0)
      draw_text(@font_huge, @info, text_x, text_y, @info_color)
    end

    def end_level(success)
      @info = success ? 'Level cleared :)' : 'Level failed :('
      @info_color = success ? Gosu::Color::BLACK : Gosu::Color::RED
    end

    def dismiss_box
      @info = nil
    end

    private

    def draw_text(font, text, x, y, color)
      font.draw(text, x, y, 1, 1.0, 1.0, color)
    end
  end
end
