
module Angrybirds
  # Enemy: pig
  class Pig
    attr_reader :shape, :body, :life_points

    RADIUS = 38
    ANIMATION_TIMEOUT = 20
    MAX_LIFE_POINTS = 1000

    def initialize(window, x, y)
      file = File.dirname(__FILE__) + '/media/pig-1.png'
      @img = Gosu::Image.new(window, file)

      @life_points = Pig::MAX_LIFE_POINTS
      @last_damage_level = 1
      @animating = false

      init_body(x, y)
      init_shape

      window.space.add_body(@body)
      window.space.add_shape(@shape)
      window.pigs.push self
    end

    def init_body(x, y)
      @mass = 50
      @inertia = 5000
      @body = CP::Body.new(@mass, @inertia)
      @body.p = CP::Vec2.new(x, y)
      @body.v = CP::Vec2.new(0, 0)
      @body.w = 0
    end

    def init_shape
      @shape = CP::Shape::Circle.new(@body, Pig::RADIUS, CP::Vec2.new(0, 0))
      @shape.u = 0.5 # friction
      @shape.e = 0.7 # elasticity
      @shape.collision_type = :pig
      @shape.object = self
    end

    def center_x
      @body.p.x
    end

    def center_y
      @body.p.y
    end

    def draw
      @img.draw_rot(@body.p.x, @body.p.y, 0, @body.a.radians_to_degrees)
    end

    def hit(damage)
      @life_points -= damage
      destroyed?
    end

    def damage_level
      5 - (@life_points / (Pig::MAX_LIFE_POINTS / 4)).ceil
    end

    def destroyed?
      @life_points < 0
    end

    def update
      @body.w *= 0.9
      @body.reset_forces

      update_damage
    end

    def update_damage
      if damage_level != @last_damage_level
        file = File.dirname(__FILE__) + "/media/pig-#{damage_level}.png"
        @img.insert(file, 0, 0)
      end

      animate if damage_level == 1

      @last_damage_level = damage_level
    end

    def animate
      animate_random if rand(1..1000) == 1 && !@animating

      @animating_timeout -= 1 if @animating

      return unless @animating_timeout == 0

      @animating = false
      @img.insert(File.dirname(__FILE__) + '/media/pig-1.png', 0, 0)
    end

    private

    def animate_random
      if rand(1..2) == 1
        animation = 'blink'
        @animating_timeout = 30
      else
        animation = 'smile'
        @animating_timeout = 100
      end

      animation_file = File.dirname(__FILE__) + "/media/pig-#{animation}.png"
      @animating = true
      @img.insert(animation_file, 0, 0)
    end
  end
end
