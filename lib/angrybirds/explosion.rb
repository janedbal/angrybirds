
module Angrybirds
  # pig explosion
  class Explosion
    def initialize(window, x, y)
      @window = window
      @x = x
      @y = y
      @explosion_state = 1
      file = File.dirname(__FILE__) + '/media/explode-1.png'
      @img = Gosu::Image.new(@window, file)
    end

    def finished?
      @explosion_state >= 24
    end

    def draw
      @img.draw_rot(@x, @y, 0, 0)
    end

    def update
      state = (@explosion_state / 3.0).ceil
      file = File.dirname(__FILE__) + "/media/explode-#{state}.png"
      @img.insert(file, 0, 0)
      @explosion_state += 1
    end
  end
end
